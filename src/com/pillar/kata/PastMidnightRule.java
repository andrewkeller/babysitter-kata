package com.pillar.kata;

import java.time.Duration;
import java.time.LocalDateTime;

public class PastMidnightRule implements PaymentRule {

    private final long hourlyRate;

    public PastMidnightRule(long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public long apply(Job job) {
        LocalDateTime midnight = DateUtils.nextMidnight(job.getStartTime());

        // Ended before midnight, therefore no payment at this rate applies
        if (job.getEndTime().isBefore(midnight)) {
            return 0;
        }

        long hours = Duration.between(midnight, job.getEndTime()).toHours();

        return hourlyRate * hours;
    }

}
