package com.pillar.kata;

import java.time.LocalDateTime;

public class DateUtils {

    // Static class
    private DateUtils() { }

    public static LocalDateTime nextMidnight(LocalDateTime dateTime) {
        // Go to next day at this time
        LocalDateTime nextDay = dateTime.plusDays(1);
        // Drop the hours/mins/secs/nanos
        LocalDateTime midnight = LocalDateTime.of(nextDay.getYear(), nextDay.getMonth(), nextDay.getDayOfMonth(), 0, 0);
        return midnight;
    }

    public static LocalDateTime earlierOf(LocalDateTime a, LocalDateTime b) {
        return a.isBefore(b) ? a : b;
    }

    public static LocalDateTime stripFractionalHours(LocalDateTime dateTime) {
        return LocalDateTime.of(dateTime.getYear(), dateTime.getMonth(), dateTime.getDayOfMonth(), dateTime.getHour(), 0);
    }

    public static LocalDateTime roundUpNearestHour(LocalDateTime dateTime) {
        LocalDateTime correctedDateTime = stripFractionalHours(dateTime);
        if (dateTime.getMinute() > 0 || dateTime.getSecond() > 0 || dateTime.getNano() > 0) {
            correctedDateTime = correctedDateTime.plusHours(1);
        }
        return correctedDateTime;
    }

}
