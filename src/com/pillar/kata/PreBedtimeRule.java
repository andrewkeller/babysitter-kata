package com.pillar.kata;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Handles calculating the payment due for the time block between start time and bed time
 */
public class PreBedtimeRule implements PaymentRule {

    private final long hourlyRate;

    public PreBedtimeRule(long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public long apply(Job job) {
        long hours;

        if (job.getBedTime().isPresent()) {
            hours = Duration.between(job.getStartTime(), job.getBedTime().get()).toHours();

        } else {
            // this rate only applies up until midnight at the latest
            LocalDateTime midnight = DateUtils.nextMidnight(job.getStartTime());
            LocalDateTime applicableTime = DateUtils.earlierOf(job.getEndTime(), midnight);
            hours = Duration.between(job.getStartTime(), applicableTime).toHours();
        }

        return hourlyRate * hours;
    }

}
