package com.pillar.kata;

import org.junit.Test;

import java.time.LocalDateTime;

public class JobTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructJobWithStartTimeAfterEndTimeThrowsException() {
        LocalDateTime startTime = LocalDateTime.of(2016, 1, 2, 12, 0, 0);
        LocalDateTime endTime = LocalDateTime.of(2016, 1, 2, 10, 0, 0);
        Job job = new Job(startTime, endTime, null);
    }

    // TODO: Write the other tests to ensure the Job class does the right things

}
