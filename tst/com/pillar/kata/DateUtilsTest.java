package com.pillar.kata;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest {

    @Test
    public void nextMidnight() {
        LocalDateTime inputDateTime = LocalDateTime.of(2016, 1, 1, 10, 30, 45, 50);
        LocalDateTime actualDateTime = DateUtils.nextMidnight(inputDateTime);
        LocalDateTime expectedDateTime = LocalDateTime.of(2016, 1, 2, 0, 0, 0);
        assertEquals(actualDateTime, expectedDateTime);
    }

    @Test
    public void earlierOfWhereFirstDateIsEarlier() {
        LocalDateTime a = LocalDateTime.of(2016, 1, 1, 10, 30, 45, 50);
        LocalDateTime b = LocalDateTime.of(2016, 1, 2, 10, 30, 45, 50);
        LocalDateTime result = DateUtils.earlierOf(a, b);
        assertEquals(a, result);
    }

    @Test
    public void earlierOfWhereSecondDateIsEarlier() {
        LocalDateTime a = LocalDateTime.of(2016, 1, 3, 0, 0, 0);
        LocalDateTime b = LocalDateTime.of(2016, 1, 2, 10, 30, 45, 50);
        LocalDateTime result = DateUtils.earlierOf(a, b);
        assertEquals(b, result);
    }

    @Test
    public void stripFractionalHours() {
        LocalDateTime dateTime = LocalDateTime.of(2016, 1, 2, 10, 30, 45, 50);
        LocalDateTime result = DateUtils.stripFractionalHours(dateTime);
        LocalDateTime expected = LocalDateTime.of(2016, 1, 2, 10, 0, 0);
        assertEquals(expected, result);
    }

    @Test
    public void roundUpNearestHourWhenThereIsFractionalHour() {
        LocalDateTime dateTime = LocalDateTime.of(2016, 1, 2, 10, 30, 45, 50);
        LocalDateTime result = DateUtils.roundUpNearestHour(dateTime);
        LocalDateTime expected = LocalDateTime.of(2016, 1, 2, 11, 0, 0);
        assertEquals(expected, result);
    }

    @Test
    public void dateTimeRemainsTheSameWhenNoFractionalHour() {
        LocalDateTime dateTime = LocalDateTime.of(2016, 1, 2, 11, 0, 0);
        LocalDateTime result = DateUtils.roundUpNearestHour(dateTime);
        LocalDateTime expected = LocalDateTime.of(2016, 1, 2, 11, 0, 0);
        assertEquals(expected, result);
    }

}
