package com.pillar.kata;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class DefaultPaymentCalculatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void startingBefore5pmThrowsException() {
        Job job = new Job(date(1, 16), date(1, 18), null);
        DefaultPaymentCalculator paymentCalculator = new DefaultPaymentCalculator();
        paymentCalculator.calculatePaymentForJob(job);
    }

    @Test(expected = IllegalArgumentException.class)
    public void endingAfter4amThrowsException() {
        Job job = new Job(date(1, 17), date(2, 5), null);
        DefaultPaymentCalculator paymentCalculator = new DefaultPaymentCalculator();
        paymentCalculator.calculatePaymentForJob(job);
    }

    @Test
    public void oneHourWithoutBedtimePayment() {
        Job job = new Job(date(1, 17), date(1, 18), null);
        assertPayment(job, DefaultPaymentCalculator.PRE_BEDTIME_RATE);
    }

    @Test
    public void allNightWithoutBedtimePayment() {
        Job job = new Job(date(1, 17), date(2, 4), null);
        long expectedPayment = (DefaultPaymentCalculator.PRE_BEDTIME_RATE * 7)
                + (DefaultPaymentCalculator.PAST_MIDNIGHT_RATE * 4);
        assertPayment(job, expectedPayment);
    }

    @Test
    public void bedtimeAt11pmAndDoneAtMidnightPayment() {
        Job job = new Job(date(1, 18), date(2, 0), date(1, 23));
        long expectedPayment = (DefaultPaymentCalculator.PRE_BEDTIME_RATE * 5) + DefaultPaymentCalculator.BEDTIME_TO_MIDNIGHT_RATE;
        assertPayment(job, expectedPayment);
    }

    @Test
    public void bedtimeAt10pmAndDoneAt4amPayment() {
        Job job = new Job(date(1, 18), date(2, 4), date(1, 22));
        long expectedPayment = (DefaultPaymentCalculator.PRE_BEDTIME_RATE * 4)
                + (DefaultPaymentCalculator.BEDTIME_TO_MIDNIGHT_RATE * 2)
                + (DefaultPaymentCalculator.PAST_MIDNIGHT_RATE * 4);
        assertPayment(job, expectedPayment);
    }

    private static void assertPayment(Job job, long expectedPayment) {
        DefaultPaymentCalculator paymentCalculator = new DefaultPaymentCalculator();
        long payment = paymentCalculator.calculatePaymentForJob(job);
        assertEquals(expectedPayment, payment);
    }

    private static LocalDateTime date(int day, int hour) {
        return LocalDateTime.of(2016, 1, day, hour, 0);
    }

}
