package com.pillar.kata;

/**
 * Entry point for the payment calculation process
 */
public interface PaymentCalculator {

    long calculatePaymentForJob(Job job);

}
