package com.pillar.kata;

import java.time.Duration;
import java.time.LocalDateTime;

public class DuringBedtimeRule implements PaymentRule {

    private final long hourlyRate;

    public DuringBedtimeRule(long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public long apply(Job job) {
        // Child never went to bed; therefore no pay at this rate
        if (!job.getBedTime().isPresent()) {
            return 0;
        }

        LocalDateTime bedTime = job.getBedTime().get();
        LocalDateTime midnight = DateUtils.nextMidnight(bedTime);

        long hours = Duration.between(bedTime, midnight).toHours();

        return hourlyRate * hours;
    }

}
