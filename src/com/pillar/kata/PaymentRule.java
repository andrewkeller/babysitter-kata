package com.pillar.kata;

/**
 * Payment Rule
 *
 * Handles some small portion of the overall payment calculation. This is typically
 * calculating the rate for a block of time where an hourly rate applies.
 */
public interface PaymentRule {

    long apply(Job job);

}
