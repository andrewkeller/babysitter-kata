package com.pillar.kata;

import java.time.LocalDateTime;

/**
 * Default rules based payment calculator for the standard hourly rates
 */
public class DefaultPaymentCalculator extends RulesBasedPaymentCalculator {

    public static final int START_HOUR_THRESHOLD = 17;
    public static final int END_HOUR_THRESHOLD = 4;

    public static final int PRE_BEDTIME_RATE = 12;
    public static final int BEDTIME_TO_MIDNIGHT_RATE = 8;
    public static final int PAST_MIDNIGHT_RATE = 16;

    public DefaultPaymentCalculator() {
        withRule(new PreBedtimeRule(PRE_BEDTIME_RATE));
        withRule(new DuringBedtimeRule(BEDTIME_TO_MIDNIGHT_RATE));
        withRule(new PastMidnightRule(PAST_MIDNIGHT_RATE));
    }

    @Override
    public long calculatePaymentForJob(Job job) {
        enforceTimeBounds(job);
        return super.calculatePaymentForJob(job);
    }

    private void enforceTimeBounds(Job job) {
        LocalDateTime start = job.getStartTime();
        LocalDateTime end = job.getEndTime();

        if (start.getHour() < START_HOUR_THRESHOLD) {
            throw new IllegalArgumentException("Start time is too early");
        }

        LocalDateTime nextDay = start.plusDays(1);
        LocalDateTime latestAcceptableTime = LocalDateTime.of(
                nextDay.getYear(), nextDay.getMonth(),
                nextDay.getDayOfMonth(), END_HOUR_THRESHOLD, 0);

        if (end.isAfter(latestAcceptableTime)) {
            throw new IllegalArgumentException("End time is too late");
        }
    }

}
