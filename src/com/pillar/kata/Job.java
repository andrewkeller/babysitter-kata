package com.pillar.kata;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Babysitting Job
 *
 * All jobs consist of a start and end time. During a babysitting job, the child may or may not go to bed.
 */
public class Job {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Optional<LocalDateTime> bedTime;

    public Job(LocalDateTime startTime, LocalDateTime endTime, LocalDateTime bedTime) {
        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("start time cannot be after end");
        }

        if (bedTime != null) {
            if (bedTime.isBefore(startTime)) {
                throw new IllegalArgumentException("bedtime cannot be before start time");
            }
            if (bedTime.isAfter(endTime)) {
                throw new IllegalArgumentException("bedtime cannot be after end time");
            }
        }

        this.startTime = DateUtils.stripFractionalHours(startTime);
        this.endTime = DateUtils.roundUpNearestHour(endTime);

        if (bedTime != null) {
            this.bedTime = Optional.of(DateUtils.stripFractionalHours(bedTime));
        } else {
            this.bedTime = Optional.empty();
        }
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public Optional<LocalDateTime> getBedTime() {
        return bedTime;
    }

}
