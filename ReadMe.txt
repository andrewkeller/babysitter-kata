Developer Notes:

Given the nature of this exercise and the ambiguity in some of the rules, I've chose to make the assumption
that a child will never go to bed past midnight.