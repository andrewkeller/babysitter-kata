package com.pillar.kata;

import java.util.ArrayList;
import java.util.List;

/**
 * Payment Calculator based on rules
 */
public class RulesBasedPaymentCalculator implements PaymentCalculator {

    private final List<PaymentRule> paymentRules;

    public RulesBasedPaymentCalculator() {
        this.paymentRules = new ArrayList<>();
    }

    public RulesBasedPaymentCalculator(List<PaymentRule> paymentRules) {
        this.paymentRules = paymentRules;
    }

    protected void withRule(PaymentRule rule) {
        this.paymentRules.add(rule);
    }

    @Override
    public long calculatePaymentForJob(Job job) {
        long total = 0;

        for (PaymentRule rule : paymentRules) {
            total += rule.apply(job);
        }

        return total;
    }

}
